module.exports = {
  // eslint-disable-next-line no-undef
  entry: ['whatwg-fetch', `./${entryPath}/js/index.js`],
  output: {
    path: `${__dirname}/dist`,
    filename: 'bundle.js',
  },
  module: {
    loaders: [
      { test: /\.js$/, loader: 'babel-loader' },
    ],
  },
};
