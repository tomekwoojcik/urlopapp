/* eslint-disable react/jsx-filename-extension */
/* eslint-disable react/react-in-jsx-scope */
// eslint-disable-next-line import/no-extraneous-dependencies
import { BrowserRouter } from 'react-router-dom';
import UrlopApp from './pages/urlopApp/urlopApp';

function App() {
  return (
    <BrowserRouter>
      <UrlopApp />
    </BrowserRouter>
  );
}

export default App;
