/* eslint-disable max-len */
/* eslint-disable max-len */
/* eslint-disable eqeqeq */
/* eslint-disable class-methods-use-this */
class Data {
  constructor(name) {
    this.databaseName = name;
  }

  setData(data) {
    localStorage.setItem(this.databaseName, JSON.stringify(data));
  }

  getData() {
    return JSON.parse(localStorage.getItem(this.databaseName));
  }

  checkData() {
    if (this.getData() == []) {
      return;
    }
    this.setData([]);
  }
}

export default Data;

// // /* eslint-disable max-len */
// // /* eslint-disable arrow-parens */
// // /* eslint-disable eqeqeq */
// // /* eslint-disable consistent-return */
// // /* eslint-disable react/prop-types */
// // import { createContext, useMemo, useState } from 'react';
// // // eslint-disable-next-line import/no-extraneous-dependencies
// // import { useNavigate } from 'react-router';
// // import Data from './localhostContext';

// // const LoginPageContext = createContext();

// // export function LoginPageProvider({ children }) {
// //   const [loginValue, setLoginValue] = useState('');
// //   const [passwordValue, setPasswordValue] = useState('');
// //   const [toggleLogin, setToggleLogin] = useState(false);
// //   const [loginError, setLoginError] = useState([]);
// //   const userData = [{
// //     id: 1111,
// //     login: 'admin',
// //     password: 'admin',
// //     name: 'Tomasz',
// //     secondName: 'Janusz',
// //     lastName: 'Wójcik',
// //     birthday: '18.02.1997',
// //     nationality: 'Polish',
// //     city: 'Bochnia',
// //     sex: 'male',
// //     userLogin: false,
// //     userLoginTimeStamp: null,
// //     userRecovery: false,
// //   }, {
// //     id: 2222,
// //     login: 'admin2',
// //     password: 'admin2',
// //     userLogin: false,
// //     userLoginTimeStamp: null,
// //     userRecovery: false,
// //   }];
// //   const data = new Data('localStorageData');
// //   data.checkData(userData);

// //   const userObj = useMemo(() => {
// //     const logData = data.getData();
// //     // eslint-disable-next-line max-len
// //     return logData.filter(el => (el.login == loginValue || el.id == loginValue) && el.password == passwordValue);
// //     // eslint-disable-next-line react-hooks/exhaustive-deps
// //   }, [loginValue, passwordValue]);

// //   const handleValue = (e, input) => input(e.target.value);
// //   const toggleValue = () => {
// //     if (loginValue) {
// //       return;
// //     }
// //     setToggleLogin(true);
// //   };

// //   const handleUser = (object) => {
// //     const [user] = object;
// //     const logData = data.getData();
// //     const dateNow = new Date();
// //     user.userLogin = true;
// //     user.userLoginTimeStamp = JSON.stringify(dateNow);
// //     const newLogData = logData.filter(el => el.id != user.id);
// //     newLogData.push(user);
// //     data.setData(newLogData);
// //   };

// //   const personError = (mes) => {
// //     if (mes) {
// //       return;
// //     }
// //     loginError.push('incorrect login details, please try again');
// //     setLoginError(loginError);
// //   };
// //   const navigate = useNavigate();
// //   const userNavigate = (page) => navigate(`${page}`);

// //   const checkUser = async () => {
// //     // eslint-disable-next-line max-len
// //     const userIs = userObj.some(el => el !== 0);
// //     personError(userIs);
// //     if (userIs == false) {
// //       console.log('break code - user is false');
// //       return;
// //     }
// //     handleUser(userObj);
// //     userNavigate('userPage');
// //   };

// //   return (
// //     // eslint-disable-next-line react/jsx-no-constructed-context-values
// //     <LoginPageContext.Provider value={{
// //       handleValue, setLoginValue, setPasswordValue, toggleValue, toggleLogin, checkUser, loginError,
// //     }}
// //     >
// //       {children}
// //     </LoginPageContext.Provider>
// //   );
// // }

// // export default LoginPageContext;
